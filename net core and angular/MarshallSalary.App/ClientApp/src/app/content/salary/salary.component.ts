import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/core/employee.service';
import { IEmployee } from 'src/app/global/interfaces/employee';
import { IPagination } from 'src/app/global/interfaces/pagination';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styles: [
  ]
})
export class SalaryComponent implements OnInit {
  public pageNumber: number = 1;
  public divisionId: any = 0;
  public positionId: any = 0;
  public grade: any = 0;

  public isLoading: boolean = false;
  public isSelected: boolean = false;
  public selected: IEmployee;

  public employee: IPagination<IEmployee> = {
    records: [],
    pagingInfo: {
      perPage: 0,
      currentPage: 0,
      totalRecords: 0,
      totalPages: 0
    }
  };

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.callEmployees();
    this.isSelected = false;
  }

  callEmployees(): void {
    this.isLoading = true;
    this.employee.records = [];
    this.employeeService.getEmployees(this.pageNumber, this.divisionId, this.positionId, this.grade)
      .subscribe(employee => {
        this.employee = employee;
        this.isLoading = false;
      },
      error => {
        console.error(error);
        this.isLoading = false;
      });
  }

  previous(): void {
    if (this.pageNumber > 1) {
      this.pageNumber -= 1;

      this.callEmployees();
      this.isSelected = false;
    }
  }

  next(): void {
    if (this.pageNumber < this.employee.pagingInfo.totalPages) {
      this.pageNumber += 1;

      this.callEmployees();
      this.isSelected = false;
    }
  }

  select(employee: IEmployee): void {
    this.isSelected = true;
    this.selected = employee;
  }

  filter(modeFilter: any): void {
    this.pageNumber = 1;
    switch(modeFilter) {
      case 1:
        this.divisionId = this.selected.division.id;
        this.positionId = 0;
        this.grade = this.selected.grade;
        this.callEmployees();
        break;
      case 2:
        this.divisionId = 0;
        this.positionId = this.selected.position.id;
        this.grade = this.selected.grade;
        this.callEmployees();
        break;
      case 3:
        this.divisionId = 0;
        this.positionId = 0;
        this.grade = this.selected.grade;
        this.callEmployees();
        break;
      case 4:
        this.divisionId = 0;
        this.positionId = 0;
        this.grade = 0;
        this.callEmployees();
        this.isSelected = false;
        break;
    }
  }
}
