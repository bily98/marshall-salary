﻿using Ardalis.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MarshallSalary.SharedKernel.Interfaces
{
    public interface IAsyncRepository<T> : IRepositoryBase<T> where T : class
    {
        Task<bool> AnyByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<bool> AnyBySpecAsync(ISpecification<T> specification, CancellationToken cancellationToken = default);
    }
}
