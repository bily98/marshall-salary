﻿using Ardalis.Result;
using MarshallSalary.Core.Dtos;
using MarshallSalary.Core.Entities;
using MarshallSalary.Core.Interfaces;
using MarshallSalary.Core.Specifications;
using MarshallSalary.SharedKernel.Entities;
using MarshallSalary.SharedKernel.Interfaces;

namespace MarshallSalary.Core.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IAsyncRepository<Employee> _employeeRepository;

        public EmployeeService(IAsyncRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<Result<PaginatedRecord<EmployeeDto>>> FindEmployees(int perPage, int pageNumber, int divisionId, int positionId, int grade)
        {
            try
            {
                var specification = new EmployeeSpecification(perPage, pageNumber, divisionId, positionId, grade);
                var employees = await _employeeRepository.ListAsync(specification);

                employees = employees.GroupBy(x => (x.EmployeeCode, x.Position.Id))
                    .Select(x => x.OrderByDescending(y => y.Id).FirstOrDefault()).ToList();

                var employeePaginated = employees.Skip((pageNumber - 1) * perPage)
                    .Take(perPage).ToList();

                var pagination = new PaginatedRecord<EmployeeDto>(employeePaginated, employees.Count, pageNumber, perPage);

                return Result<PaginatedRecord<EmployeeDto>>.Success(pagination);
            }
            catch (Exception ex)
            {
                return Result<PaginatedRecord<EmployeeDto>>.Error(ex.StackTrace, ex.Message);
            }
        }

        public async Task<Result<bool>> SaveSalary(List<SalaryDto> salaries)
        {
            string employeeCode = salaries.FirstOrDefault().EmployeeCode;

            var specification = new EmployeeByCodeSpecification(employeeCode);
            var employee = await _employeeRepository.GetBySpecAsync(specification);

            try
            {
                foreach (var item in salaries)
                {
                    var zoned = item.SalaryDate.InUtc();

                    Employee emp = new Employee()
                    {
                        Year = zoned.Year,
                        Month = zoned.Month,
                        OfficeId = employee.OfficeId,
                        EmployeeCode = employee.EmployeeCode,
                        EmployeeName = employee.EmployeeName,
                        EmployeeSurname = employee.EmployeeSurname,
                        DivisionId = employee.DivisionId,
                        PositionId = employee.PositionId,
                        Grade = employee.Grade,
                        BeginGrade = employee.BeginGrade,
                        Birthday = employee.Birthday,
                        IdentificationNumber = employee.IdentificationNumber,
                        BaseSalary = item.BaseSalary,
                        ProductionBonus = item.ProductionBonus,
                        CompensationBonus = item.CompensationBonus,
                        Commission = item.Commission,
                        Contributions = item.Contributions
                    };

                    await _employeeRepository.AddAsync(emp);
                }

                return Result<bool>.Success(true);
            }
            catch (Exception ex)
            {
                return Result<bool>.Error(ex.StackTrace, ex.Message);
            }
        }
    }
}
