﻿using NodaTime;
using NodaTime.Serialization.SystemTextJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MarshallSalary.SharedKernel.Extensions
{
    public static class JsonExtensions
    {
        private static readonly JsonSerializerOptions _jsonOptions = ConfigureOptions(new JsonSerializerOptions());

        public static JsonSerializerOptions ConfigureOptions(JsonSerializerOptions serializerOptions)
        {
            serializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            serializerOptions.PropertyNameCaseInsensitive = true;
            serializerOptions.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;

            // Configures JsonSerializer to serialize enums as Strings.
            serializerOptions.Converters.Add(new JsonStringEnumConverter());

            // Configures JsonSerializer to properly serialize NodaTime types.
            serializerOptions.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb);

            return serializerOptions;
        }

        public static T? FromJson<T>(this string json)
        {
            return JsonSerializer.Deserialize<T>(json, _jsonOptions);
        }

        public static string ToJson(this object obj)
        {
            return JsonSerializer.Serialize(obj, _jsonOptions);
        }
    }
}
