﻿using MarshallsEmployee.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MarshallsEmployee
{
    public partial class _Default : Page
    {
        private readonly MarshallSalaryEntities _marshallSalaryEntities = new MarshallSalaryEntities();

        List<employee> employees = new List<employee>();
        bool breakLoop = false;
        decimal employeeBonus = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            GvEmployee.DataSource = employees;
            GvEmployee.DataBind();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            employeeBonus = 0;
            string employeeCode = TxtEmployeeCode.Text;

            employees = _marshallSalaryEntities.employee
                .Include(x => x.division)
                .Include(x => x.office)
                .Include(x => x.position)
                .Where(x => x.employee_code == employeeCode)
                .OrderByDescending(x => x.year)
                .ThenByDescending(x => x.month).ToList();

            GvEmployee.DataSource = employees;
            GvEmployee.DataBind();
        }

        protected void GvEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var data = e.Row.DataItem as employee;

            if (data != null & !breakLoop)
            {
                if (FindConsecutive(data.id))
                {
                    employeeBonus += data.base_salary;
                    e.Row.CssClass = "table-warning";

                    
                }
            }

            if (e.Row.RowIndex == (employees.Count - 1))
            {
                employeeBonus /= 3;
                TxtEmployeeBonus.Text = $"{decimal.Round(employeeBonus, 2)}";
            }
        }

        private bool FindConsecutive(int id)
        {
            employee employee = employees.FirstOrDefault();
            int n = (employees.Count >= 3 ? 3 : employees.Count);

            for (int i = 0; i < n; i++)
            {
                if ((employees[i].year == ((employee.month == 1) ? employee.year - 1 : employee.year)) & 
                    (employees[i].month == ((employee.month == 1) ? 12 : employee.month - ((i == 0) ? 0 : 1))) &
                    (employees[i].id == id))
                {
                    return true;
                }

                employee = employees[i];
            }

            breakLoop = true;
            return false;
        }
    }
}