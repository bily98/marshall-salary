﻿namespace MarshallSalary.App.Endpoints
{
    public class Routes
    {
        private const string ApiRoot = "/api";

        public static class Employee
        {
            public const string GetEmployees = ApiRoot + "/employees";
            public const string GetIfHaveSalary = ApiRoot + "/salary";
            public const string PostSalary = ApiRoot + "/salary";
        }
    }
}
