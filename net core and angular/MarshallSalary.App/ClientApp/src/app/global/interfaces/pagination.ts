export interface IPagination<T> {
    records: T[];
    pagingInfo: IPagingInfo;
}

export interface IPagingInfo {
    perPage: number;
    currentPage: number;
    totalRecords: number;
    totalPages: number;
}
