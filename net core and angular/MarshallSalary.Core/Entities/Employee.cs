﻿using MarshallSalary.SharedKernel.Entities;
using NodaTime;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshallSalary.Core.Entities
{
    public class Employee : EntityBase
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int OfficeId { get; set; }
        [StringLength(10)]
        public string EmployeeCode { get; set; }
        [StringLength(150)]
        public string EmployeeName { get; set; }
        [StringLength(150)]
        public string EmployeeSurname { get; set; }
        public int DivisionId { get; set; }
        public int PositionId { get; set; }
        public int Grade { get; set; }
        public Instant BeginGrade { get; set; }
        public Instant Birthday { get; set; }
        [StringLength(10)]
        public string IdentificationNumber { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal BaseSalary { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal ProductionBonus { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal CompensationBonus { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Commission { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Contributions { get; set; }

        public virtual Office Office { get; set; }
        public virtual Division Division { get; set; }
        public virtual Position Position { get; set; }
    }
}
