﻿using MarshallSalary.Infrastructure.Data;
using MarshallSalary.Infrastructure.Importer;

namespace MarshallSalary.App.Seeds
{
    public static class SeedData
    {
        public static async Task InitializeAsync(SalaryDbContext dbContext, IHostEnvironment env, Importer importer, bool addTestEntities = false)
        {
            await importer.Run();
        }
    }
}
