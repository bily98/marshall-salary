﻿using MarshallSalary.Core.Entities;
using MarshallSalary.SharedKernel.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MarshallSalary.Infrastructure.Importer
{
    public class Importer
    {
        private readonly IConfiguration _configuration;
        private readonly IAsyncRepository<Division> _divisionRepository;
        private readonly IAsyncRepository<Employee> _employeeRepository;
        private readonly IAsyncRepository<Office> _officeRepository;
        private readonly IAsyncRepository<Position> _positionRepository;

        public Importer(IConfiguration configuration, IAsyncRepository<Division> divisionRepository,
            IAsyncRepository<Employee> employeeRepository, IAsyncRepository<Office> officeRepository,
            IAsyncRepository<Position> positionRepository, ILogger<Importer> logger)
        {
            _configuration = configuration;
            _divisionRepository = divisionRepository;
            _employeeRepository = employeeRepository;
            _officeRepository = officeRepository;
            _positionRepository = positionRepository;
        }

        public async Task Run()
        {
            await CreateDivisions("./Resources/division.csv");
            await CreateOffices("./Resources/office.csv");
            await CreatePositions("./Resources/position.csv");
            await CreateEmployees("./Resources/employee.csv");
        }

        private async Task CreateDivisions(string path)
        {
            var parsed = CsvImporter<Division>.FromCsvPath(path).ToList();
            List<Division> divisions = await _divisionRepository.ListAsync();

            foreach (Division division in parsed)
            {
                var existingDivision = divisions.FirstOrDefault(x => x.Name == division.Name);

                if (existingDivision == null)
                {
                    division.Id = 0;
                    await _divisionRepository.AddAsync(division);
                }
            }
        }

        private async Task CreateEmployees(string path)
        {
            var parsed = CsvImporter<Employee>.FromCsvPath(path).ToList();
            List<Employee> employees = await _employeeRepository.ListAsync();

            foreach (Employee employee in parsed)
            {
                var existingEmployee = employees.FirstOrDefault(x => x.EmployeeCode == employee.EmployeeCode);

                if (existingEmployee == null)
                {
                    employee.Id = 0;
                    employee.Division = null;
                    employee.Office = null;
                    employee.Position = null;

                    await _employeeRepository.AddAsync(employee);
                }
            }
        }

        private async Task CreateOffices(string path)
        {
            var parsed = CsvImporter<Office>.FromCsvPath(path).ToList();
            List<Office> offices = await _officeRepository.ListAsync();

            foreach (Office office in parsed)
            {
                var existingOffice = offices.FirstOrDefault(x => x.Name == office.Name);

                if (existingOffice == null)
                {
                    office.Id = 0;
                    await _officeRepository.AddAsync(office);
                }
            }
        }

        private async Task CreatePositions(string path)
        {
            var parsed = CsvImporter<Position>.FromCsvPath(path).ToList();
            List<Position> positions = await _positionRepository.ListAsync();

            foreach (Position position in parsed)
            {
                var existingPosition = positions.FirstOrDefault(x => x.Name == position.Name);

                if (existingPosition == null)
                {
                    position.Id = 0;
                    await _positionRepository.AddAsync(position);
                }
            }
        }
    }
}
