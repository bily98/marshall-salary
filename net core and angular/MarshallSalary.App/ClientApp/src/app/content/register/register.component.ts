import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/core/employee.service';
import { ISalary } from 'src/app/global/interfaces/employee';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit, OnDestroy {
  public code: number;
  public formSalary: FormGroup;

  private sub: any;

  public salaries: ISalary[] = [];

  constructor(private fb: FormBuilder, private route: ActivatedRoute,
    private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.code = +params['code'];
   });

    this.formSalary = this.fb.group({
      employeeCode: this.code,
      salaryDate: new Date(),
      baseSalary: 0.0,
      productionBonus: 0.0,
      compensationBonus: 0.0,
      commission: 0.0,
      contributions: 0.0
    });
  }

  add(): void {
    const salary: ISalary = Object.assign({}, this.formSalary.value);
    
    this.employeeService.getSalary(salary.employeeCode, salary.salaryDate).subscribe(() => {
      this.salaries.push(salary);
    }, error => alert(error.error));
  }

  save(): void {
    if (this.salaries.length > 0) {
      this.employeeService.setSalary(this.salaries).subscribe(() => {
        alert('Saved Successfully!');
        this.router.navigate(['/salary']);
      }, error => alert('Something went wrong!'));
    } else {
      alert('Add salary to table!')
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
