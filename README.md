# Marshall Salary
This repository contains an ASP.NET application with WebForms And ASP.NET Core with Angular.

## ASP.NET application with WebForms
This app allows you to search for employees and display their salary information. It also calculates the Bonus for the employee, according to the last 3 consecutive months.

## ASP.NET Core with Angular
This app is on DDD and Clean Architecture using specification and repository patterns. This application allows you to view the list of employees along with their last total salary, making filters for them. It also allows you to add information about an employee's salary.