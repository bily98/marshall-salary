﻿using MarshallSalary.Core.Dtos;
using Ardalis.ApiEndpoints;
using MarshallSalary.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using MarshallSalary.App.Requests;
using MarshallSalary.SharedKernel.Entities;

namespace MarshallSalary.App.Endpoints.Employees
{
    public class GetEmployees : EndpointBaseAsync.WithRequest<GetEmployeesRequest>.WithActionResult<EmployeeDto>
    {
        private readonly IEmployeeService _employeeService;

        public GetEmployees(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet(Routes.Employee.GetEmployees)]
        public override async Task<ActionResult<EmployeeDto>> HandleAsync([FromQuery] GetEmployeesRequest request, CancellationToken cancellationToken = default)
        {
            var result = await _employeeService.FindEmployees(request.PerPage, request.PageNumber,
                request.DivisionId, request.PositionId, request.Grade);

            if (result.IsSuccess)
            {
                return Ok(result.Value);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }
    }
}
