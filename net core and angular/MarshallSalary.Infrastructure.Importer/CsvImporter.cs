﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.Extensions.Logging;
using NodaTime;
using System.Globalization;
using System.Text;

namespace MarshallSalary.Infrastructure.Importer
{
    public class CsvImporter<T>
    {
        public static List<T> FromCsvPath(string path)
        {
            using var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path), Encoding.GetEncoding("iso-8859-1"));
            return ReadProperties(reader);
        }

        public static List<T> FromString(string csvString)
        {
            using var reader = new StringReader(csvString);
            return ReadProperties(reader);
        }

        private static List<T> ReadProperties(TextReader reader)
        {
            CsvConfiguration csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ",",
                Encoding = Encoding.GetEncoding("iso-8859-1"),
                DetectDelimiter = true,
                BadDataFound = null,
                MissingFieldFound = null,
            };

            using (var csv = new CsvReader(reader, csvConfiguration))
            {
                csv.Read();
                csv.ReadHeader();
                csv.Context.TypeConverterCache.AddConverter<Instant>(new InstantConverter());

                return csv.GetRecords<T>().ToList();
            }
        }
    }

    public class InstantConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (!string.IsNullOrEmpty(text))
            {
                DateTime dateTime = Convert.ToDateTime(text);
                Instant instant = Instant.FromDateTimeOffset(dateTime);

                return instant;
            }

            return base.ConvertFromString(text, row, memberMapData);
        }
    }
}
