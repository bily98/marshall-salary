﻿using NodaTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarshallSalary.Core.Dtos
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeFullName { get; set; }
        public DivisionDto Division { get; set; }
        public PositionDto Position { get; set; }
        public int Grade { get; set; }
        public Instant BeginGrade { get; set; }
        public Instant Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
    }
}
