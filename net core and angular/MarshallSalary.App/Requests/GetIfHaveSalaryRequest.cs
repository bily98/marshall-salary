﻿using NodaTime;

namespace MarshallSalary.App.Requests
{
    public class GetIfHaveSalaryRequest
    {
        public string EmployeeCode { get; set; }
        public Instant SalaryDate { get; set; }
    }
}
