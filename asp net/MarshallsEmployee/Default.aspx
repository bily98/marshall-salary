﻿<%@ Page Title="Search" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MarshallsEmployee._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-fluid p-3">
        <div class="jumbotron">
          <h1 class="display-4">Search Employee</h1>
            <div class="row mt-3">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="employee-code">Employee Code</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="TxtEmployeeCode"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="employee-code">Employee Bonus</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="TxtEmployeeBonus" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <asp:Button runat="server" ID="BtnSearch" Text="Search" CssClass="btn btn-primary float-float-right" OnClick="BtnSearch_Click"/>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView runat="server" ID="GvEmployee" CssClass="table table-hover" AutoGenerateColumns="false" OnRowDataBound="GvEmployee_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="year" HeaderText="Year" />
                            <asp:BoundField DataField="month" HeaderText="Month" />
                            <asp:BoundField DataField="office.name" HeaderText="Office" />
                            <asp:BoundField DataField="employee_name" HeaderText="Name" />
                            <asp:BoundField DataField="employee_surname" HeaderText="Surname" />
                            <asp:BoundField DataField="division.name" HeaderText="Division" />
                            <asp:BoundField DataField="position.name" HeaderText="Position" />
                            <asp:BoundField DataField="grade" HeaderText="Grade" />
                            <asp:BoundField DataField="begin_grade" HeaderText="Begin Grade" />
                            <asp:BoundField DataField="birthday" HeaderText="Birthday" />
                            <asp:BoundField DataField="identification_number" HeaderText="Identification Number" />
                            <asp:BoundField DataField="base_salary" HeaderText="Base Salary" />
                            <asp:BoundField DataField="production_bonus" HeaderText="Production Bonus" />
                            <asp:BoundField DataField="compensation_bonus" HeaderText="Compensation Bonus" />
                            <asp:BoundField DataField="commission" HeaderText="Commission" />
                            <asp:BoundField DataField="contributions" HeaderText="Contributions" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
