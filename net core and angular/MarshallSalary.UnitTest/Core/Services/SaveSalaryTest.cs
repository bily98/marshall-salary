﻿using MarshallSalary.Core.Dtos;
using MarshallSalary.Core.Entities;
using MarshallSalary.Core.Specifications;
using MarshallSalary.SharedKernel.Interfaces;
using MarshallSalary.UnitTest.Common;
using Moq;
using NodaTime;
using NodaTime.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace MarshallSalary.UnitTest.Core.Services
{
    public class SaveSalaryTest
    {
        public static string EmployeeCode { get; set; } = "10000001";
        public static FakeClock Clock = new FakeClock(Instant.FromUtc(2022, 2, 13, 0, 0));

        public static Employee Employee = new Employee()
        {
            Id = 1,
            EmployeeCode = EmployeeCode
        };

        public static List<SalaryDto> Salaries = new List<SalaryDto>()
        {
            new SalaryDto()
            {
                EmployeeCode = EmployeeCode,
                SalaryDate = Clock.GetCurrentInstant(),
                BaseSalary = 0,
                ProductionBonus = 0,
                CompensationBonus = 0,
                Commission = 0,
                Contributions = 0
            }
        };

        [Fact]
        public async Task WhenModelIsValid_InsertSalaryAsync()
        {
            Mock<IAsyncRepository<Employee>> employeeRepository;

            var employeeService = ServiceUtilities.CreateEmployeeService(out employeeRepository);

            employeeRepository.Setup(x => x.GetBySpecAsync(
                It.IsAny<EmployeeByCodeSpecification>(),
                It.IsAny<CancellationToken>()))
                .ReturnsAsync(Employee);

            employeeRepository.Setup(x => x.AddAsync(
                It.IsAny<Employee>(),
                It.IsAny<CancellationToken>()))
                .ReturnsAsync(Employee);

            var result = await employeeService.SaveSalary(Salaries);

            Assert.True(result.IsSuccess);
            Assert.True(result.Value);
        }
    }
}
