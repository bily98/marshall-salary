using MarshallSalary.App.Configurations.Extensions;
using Serilog;

namespace MarshallSalary.App
{
    public class Program
    {
        private const string AppName = "Marshall Salary";

        public static async Task Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
                .ConfigureBaseLogging(AppName)
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                var host = CreateHostBuilder(args).Build();

                host.MigrateDatabase();
                await host.SeedDatabaseAsync();

                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
