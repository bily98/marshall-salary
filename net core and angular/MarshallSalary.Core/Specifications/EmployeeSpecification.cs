﻿using Ardalis.Specification;
using MarshallSalary.Core.Dtos;
using MarshallSalary.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace MarshallSalary.Core.Specifications
{
    public class EmployeeSpecification : Specification<Employee, EmployeeDto>
    {
        public EmployeeSpecification(int perPage, int pageNumber, int divisionId, int positionId, int grade)
        {
            Query.Include(x => x.Division)
                .Include(x => x.Office)
                .Include(x => x.Position);

            if (divisionId != 0 & grade != 0)
            {
                Query.Where(x => x.DivisionId == divisionId & x.Grade == grade);
            }
            else if (positionId != 0 & grade != 0)
            {
                Query.Where(x => x.PositionId == positionId & x.Grade == grade);
            }
            else if (grade != 0)
            {
                Query.Where(x => x.Grade == grade);
            }

            Query.OrderBy(x => x.EmployeeName);

            Query.Select(x => new EmployeeDto()
            {
                Id = x.Id,
                EmployeeCode = x.EmployeeCode,
                EmployeeFullName = $"{x.EmployeeName} {x.EmployeeSurname}",
                Division = new DivisionDto() { Id = x.Division.Id, Name = x.Division.Name },
                Position = new PositionDto() { Id = x.Position.Id, Name = x.Position.Name },
                Grade = x.Grade,
                BeginGrade = x.BeginGrade,
                Birthday = x.Birthday,
                IdentificationNumber = x.IdentificationNumber,
                TotalSalary = TotalSalary(x)
            });
        }

        private static decimal TotalSalary(Employee employee)
        {
            var otherIncome = (employee.BaseSalary + employee.Commission) * 0.08m + employee.Commission;
            var totalSalary = employee.BaseSalary + employee.ProductionBonus + 
                (employee.CompensationBonus * 0.75m) + otherIncome - employee.Contributions;

            return totalSalary;
        }
    }
}
