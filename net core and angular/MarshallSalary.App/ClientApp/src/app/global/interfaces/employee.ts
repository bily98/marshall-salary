import { IDivision } from "./division";
import { IPosition } from "./position";

export interface IEmployee {
  id: number;
  employeeCode: string;
  employeeFullName: string;
  division: IDivision;
  position: IPosition;
  grade: number;
  beginGrade: Date;
  birthday: Date;
  identificationNumber: string;
  totalSalary: number;
}

export interface ISalary {
  employeeCode: string;
  salaryDate: Date;
  baseSalary: number;
  productionBonus: number;
  compensationBonus: number;
  commission: number;
  contributions: number;
}
