﻿using MarshallSalary.Core.Entities;
using MarshallSalary.Core.Services;
using MarshallSalary.SharedKernel.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarshallSalary.UnitTest.Common
{
    public static class ServiceUtilities
    {
        public static EmployeeService CreateEmployeeService(out Mock<IAsyncRepository<Employee>> employeeRepository)
        {
            employeeRepository = new();

            return new EmployeeService(employeeRepository.Object);
        }
    }
}
