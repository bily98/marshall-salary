﻿using Ardalis.Specification;
using System.Collections;
using System.Linq.Expressions;

namespace MarshallSalary.SharedKernel.Extensions
{
    public static class DynamicExtensions
    {
        public static ISpecificationBuilder<TSource> ThenBy<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName)
        {
            return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.ThenBy);
        }

        public static ISpecificationBuilder<TSource> ThenBy<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, Expression<Func<TSource, object>> keySelector)
        {
            ((List<(Expression<Func<TSource, object>>, OrderTypeEnum)>)specificationBuilder.Specification.OrderExpressions)
                    .Add((keySelector, OrderTypeEnum.ThenBy));

            return specificationBuilder;
        }

        public static ISpecificationBuilder<TSource> OrderBy<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName)
        {
            return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.OrderBy);
        }

        public static ISpecificationBuilder<TSource> ThenBy<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName,
            bool isDescending)
        {
            if (isDescending)
                return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.ThenByDescending);
            else
                return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.ThenBy);
        }

        public static ISpecificationBuilder<TSource> OrderBy<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName,
            bool isDescending)
        {
            if (isDescending)
                return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.OrderByDescending);
            else
                return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.OrderBy);
        }

        public static ISpecificationBuilder<TSource> ThenByDescending<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName)
        {
            return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.ThenByDescending);
        }

        public static ISpecificationBuilder<TSource> OrderByDescending<TSource>(this ISpecificationBuilder<TSource> specificationBuilder,
            string propertyName)
        {
            return BuildOrderQuery(specificationBuilder, propertyName, OrderTypeEnum.OrderByDescending);
        }

        public static ISpecificationBuilder<TSource> Where<TSource>(this ISpecificationBuilder<TSource> specificationBuilder, string propertyName, string criteria)
        {
            return BuildWhereQuery(specificationBuilder, propertyName, criteria);
        }

        static ISpecificationBuilder<TSource> BuildOrderQuery<TSource>(ISpecificationBuilder<TSource> specificationBuilder,
            string propertyName, OrderTypeEnum orderTypeEnum)
        {
            // Initalized entity type
            var entityType = typeof(TSource);

            // Declare a type for lambda expression
            Type funcType = typeof(Func<,>).MakeGenericType(typeof(TSource), typeof(object));

            // Getting a info of property if exists in entity
            var propertyInfo = entityType.GetProperty(propertyName);
            if (propertyInfo == null)
                throw new ArgumentOutOfRangeException(nameof(propertyName), "Unknown column " + propertyName);

            // Is property is a collection
            bool isCollection = propertyInfo.PropertyType.GetInterfaces()
                                        .Any(x => x == typeof(IEnumerable));

            // Declare the argument and property of entity for lambda expression
            var arg = Expression.Parameter(entityType, "x"); // x =>
            var property = Expression.Property(arg, propertyName); // x.Property

            // Validating if is a collection
            if (isCollection && propertyInfo.PropertyType != typeof(string))
            {
                // Declare a type for secondary lambda expression
                var type = propertyInfo.PropertyType.GenericTypeArguments[0];
                //var argY = Expression.Parameter(type, "y"); 
                //var propertyY = Expression.Property(argY, columns[1]);

                var firstOrDefaultType = Expression.Call(typeof(Enumerable), "FirstOrDefault", new[] { type }, property);
                var firstOrDefaultSelector = Expression.Lambda(funcType, Expression.Convert(firstOrDefaultType, typeof(object)), new ParameterExpression[] { arg });

                //((List<(Expression<Func<TSource, object>>, OrderTypeEnum)>)specificationBuilder.Specification.OrderExpressions)
                //    .Add(((Expression<Func<TSource, object>>)firstOrDefaultSelector, orderTypeEnum));
            }
            else
            {
                // Create a lambda expression
                var selector = Expression.Lambda(funcType, Expression.Convert(property, typeof(object)), new ParameterExpression[] { arg }); // x => x.Property

                // Add lambda expression to OrderExpressions Specification
                ((List<(Expression<Func<TSource, object>>, OrderTypeEnum)>)specificationBuilder.Specification.OrderExpressions)
                    .Add(((Expression<Func<TSource, object>>)selector, orderTypeEnum));
            }

            return specificationBuilder;
        }

        static ISpecificationBuilder<TSource> BuildWhereQuery<TSource>(ISpecificationBuilder<TSource> specificationBuilder, string propertyName, string criteria)
        {
            // Initalized entity type
            var entityType = typeof(TSource);

            // Declare a type for lambda expression
            Type funcType = typeof(Func<,>).MakeGenericType(typeof(TSource), typeof(bool));

            // Getting a info of property if exists in entity
            var propertyInfo = entityType.GetProperty(propertyName);
            if (propertyInfo == null)
                throw new ArgumentOutOfRangeException(nameof(propertyName), "Unknown column " + propertyName);

            // Is property is a collection
            bool isCollection = propertyInfo.PropertyType.GetInterfaces()
                                        .Any(x => x == typeof(IEnumerable));

            // Declare the argument and property of entity for lambda expression
            var arg = Expression.Parameter(entityType, "x"); // x =>
            var property = Expression.Property(arg, propertyName); // x.Property

            if (isCollection && propertyInfo.PropertyType != typeof(string))
            {

            }
            else
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                    var body = Expression.Call(property, containsMethod, Expression.Constant(criteria));
                    var where = Expression.Lambda<Func<TSource, bool>>(body, arg);

                    // Add lambda expression to WhereExpressions Specification
                    ((List<Expression<Func<TSource, bool>>>)(specificationBuilder.Specification.WhereExpressions))
                        .Add(where);
                }
                else if (propertyInfo.PropertyType == typeof(int))
                {
                    var toStringMethod = typeof(int).GetMethod("ToString", new Type[] { });
                    var toStringExpr = Expression.Call(property, toStringMethod);
                    var equalsMethod = typeof(string).GetMethod("Equals", new[] { typeof(string) });
                    var body = Expression.Call(toStringExpr, equalsMethod, Expression.Constant(criteria));
                    var where = Expression.Lambda<Func<TSource, bool>>(body, arg);

                    // Add lambda expression to WhereExpressions Specification
                    ((List<Expression<Func<TSource, bool>>>)(specificationBuilder.Specification.WhereExpressions))
                        .Add(where);
                }

            }

            return specificationBuilder;
        }
    }
}
