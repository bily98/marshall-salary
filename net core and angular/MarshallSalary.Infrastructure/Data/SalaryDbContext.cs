﻿using MarshallSalary.Core.Entities;
using MarshallSalary.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;

namespace MarshallSalary.Infrastructure.Data
{
    public class SalaryDbContext : DbContext
    {
        public SalaryDbContext()
        {

        }

        public SalaryDbContext(DbContextOptions<SalaryDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Division> Divisions { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<Position> Positions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SetSimpleUnderscoreTableNameConvention(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}
