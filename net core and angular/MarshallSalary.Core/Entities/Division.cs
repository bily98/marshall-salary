﻿using MarshallSalary.SharedKernel.Entities;
using System.ComponentModel.DataAnnotations;

namespace MarshallSalary.Core.Entities
{
    public class Division : EntityBase
    {
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
