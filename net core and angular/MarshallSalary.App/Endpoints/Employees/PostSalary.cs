﻿using Ardalis.ApiEndpoints;
using AutoMapper;
using MarshallSalary.App.Requests;
using MarshallSalary.Core.Dtos;
using MarshallSalary.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MarshallSalary.App.Endpoints.Employees
{
    public class PostSalary : EndpointBaseAsync.WithRequest<PostSalaryRequest>.WithActionResult
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public PostSalary(IEmployeeService employeeService, IMapper mapper)
        {
            _employeeService = employeeService;
            _mapper = mapper;
        }

        [HttpPost(Routes.Employee.PostSalary)]
        public override async Task<ActionResult> HandleAsync(PostSalaryRequest? request, CancellationToken cancellationToken = default)
        {
            var mapped = _mapper.Map<List<SalaryRequest>, List<SalaryDto>>(request.Salaries);

            var result = await _employeeService.SaveSalary(mapped);

            if (result.IsSuccess)
            {
                return Created(Routes.Employee.PostSalary, result.Value);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }
    }
}
