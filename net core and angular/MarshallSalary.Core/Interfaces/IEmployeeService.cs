﻿using Ardalis.Result;
using MarshallSalary.Core.Dtos;
using MarshallSalary.SharedKernel.Entities;

namespace MarshallSalary.Core.Interfaces
{
    public interface IEmployeeService
    {
        Task<Result<PaginatedRecord<EmployeeDto>>> FindEmployees(int perPage, int pageNumber, int divisionId, int positionId, int grade);
        Task<Result<bool>> SaveSalary(List<SalaryDto> salaries);
    }
}
