﻿using Ardalis.Specification;
using MarshallSalary.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarshallSalary.Core.Specifications
{
    public class EmployeeByCodeSpecification : Specification<Employee>, ISingleResultSpecification
    {
        public EmployeeByCodeSpecification(string employeeCode)
        {
            Query.Where(x => x.EmployeeCode == employeeCode);
        }
    }
}
