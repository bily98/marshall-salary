﻿namespace MarshallSalary.App.Requests
{
    public class GetEmployeesRequest
    {
        public int PerPage { get; set; } = 10;
        public int PageNumber { get; set; } = 1;
        public int DivisionId { get; set; } = 0;
        public int PositionId { get; set; } = 0;
        public int Grade { get; set; } = 0;
    }
}
