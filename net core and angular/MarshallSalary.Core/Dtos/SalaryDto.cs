﻿using NodaTime;

namespace MarshallSalary.Core.Dtos
{
    public class SalaryDto
    {
        public string EmployeeCode { get; set; }
        public Instant SalaryDate { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
    }
}
