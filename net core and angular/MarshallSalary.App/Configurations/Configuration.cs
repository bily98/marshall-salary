﻿using MarshallSalary.Core.Interfaces;
using MarshallSalary.Core.Services;
using MarshallSalary.Infrastructure.Data;
using MarshallSalary.Infrastructure.Importer;
using MarshallSalary.Infrastructure.Repositories;
using MarshallSalary.SharedKernel.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

namespace MarshallSalary.App.Configurations
{
    public static class Configuration
    {
        public static IServiceCollection AddDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SalaryDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("devConnection"),
                    x =>
                    {
                        x.UseNodaTime();
                        x.MigrationsAssembly("MarshallSalary.Infrastructure.Migrations");
                    })
                    .UseSnakeCaseNamingConvention());

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            // Employee
            services.AddTransient<IEmployeeService, EmployeeService>();

            // Importer
            services.AddTransient<Importer>();

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IAsyncRepository<>), typeof(AsyncRepository<>));

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Marshalls Salary",
                    Description = "Marshalls Salary API",
                    Contact = new OpenApiContact
                    {
                        Name = "Bily Venerio",
                        Email = "bilyjv98.16@gmail.com"
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use for Marshalls Salary App",
                    }
                });

                c.EnableAnnotations();
            });

            return services;
        }
    }
}
