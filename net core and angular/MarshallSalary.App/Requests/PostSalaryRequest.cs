﻿using Microsoft.AspNetCore.Mvc;
using NodaTime;

namespace MarshallSalary.App.Requests
{
    public class PostSalaryRequest
    {
        [FromBody] public List<SalaryRequest> Salaries { get; set; }
    }

    public class SalaryRequest
    {
        public string EmployeeCode { get; set; }
        public DateTime SalaryDate { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
    }
}
