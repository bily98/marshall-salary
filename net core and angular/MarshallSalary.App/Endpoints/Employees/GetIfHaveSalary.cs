﻿using Ardalis.ApiEndpoints;
using MarshallSalary.App.Requests;
using MarshallSalary.Core.Entities;
using MarshallSalary.Core.Specifications;
using MarshallSalary.SharedKernel.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace MarshallSalary.App.Endpoints.Employees
{
    public class GetIfHaveSalary : EndpointBaseAsync.WithRequest<GetIfHaveSalaryRequest>.WithActionResult
    {
        private readonly IAsyncRepository<Employee> _employeeRepository;

        public GetIfHaveSalary(IAsyncRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet(Routes.Employee.GetIfHaveSalary)]
        public override async Task<ActionResult> HandleAsync([FromQuery] GetIfHaveSalaryRequest request, CancellationToken cancellationToken = default)
        {
            var specification = new ExistSalarySpecification(request.EmployeeCode, request.SalaryDate);
            var employees = await _employeeRepository.ListAsync(specification);

            if (employees.Count > 0)
            {
                return BadRequest("This employee have a Salary at this date");
            }
            else
            {
                return Ok();
            }
        }
    }
}
