import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmployee, ISalary } from '../global/interfaces/employee';
import { IPagination } from '../global/interfaces/pagination';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getEmployees(pageNumber: any, divisionId: any = 0, positionId: any = 0, grade: any = 0): Observable<IPagination<IEmployee>> {
    return this.http.get<IPagination<IEmployee>>(`${this.baseUrl}api/employees?pageNumber=${pageNumber}&divisionId=${divisionId}&positionId=${positionId}&grade=${grade}`);
  }

  getSalary(employeeCode: any, salaryDate: any) {
    return this.http.get(`${this.baseUrl}api/salary?employeeCode=${employeeCode}&salaryDate=${salaryDate}T06:00:00Z`);
  }

  setSalary(salaries: ISalary[]) {
    let body = {
      salaries
    }
    return this.http.post(`${this.baseUrl}api/salary`, body);
  }
}
