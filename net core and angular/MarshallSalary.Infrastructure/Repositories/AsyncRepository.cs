﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using MarshallSalary.Infrastructure.Data;
using MarshallSalary.SharedKernel.Entities;
using MarshallSalary.SharedKernel.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace MarshallSalary.Infrastructure.Repositories
{
    public class AsyncRepository<T> : RepositoryBase<T>, IAsyncRepository<T> where T : EntityBase
    {
        private readonly SalaryDbContext _context;

        public AsyncRepository(SalaryDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<bool> AnyByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await _context.Set<T>().AnyAsync(e => e.Id == id, cancellationToken);
        }

        public async Task<bool> AnyBySpecAsync(ISpecification<T> specification, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(specification);
            return await specificationResult.AnyAsync(cancellationToken);
        }
    }
}
