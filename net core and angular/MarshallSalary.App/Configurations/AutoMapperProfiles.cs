﻿using AutoMapper;
using MarshallSalary.App.Requests;
using MarshallSalary.Core.Dtos;
using NodaTime;
using NodaTime.Extensions;

namespace MarshallSalary.App.Configurations
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<SalaryRequest, SalaryDto>()
                .ForMember(dest => dest.SalaryDate, opt => opt.MapFrom(src => Instant.FromDateTimeUtc(DateTime.SpecifyKind(src.SalaryDate, DateTimeKind.Utc)))).ReverseMap();
        }
    }
}
