﻿namespace MarshallSalary.SharedKernel.Entities
{
    public class PaginatedRecord<T>
    {
        private PaginatedRecord()
        {
        }

        public PaginatedRecord(List<T> records, int pageNumber = 1, int perPage = 10)
        {
            perPage = pageNumber == 0 ? 0 : perPage;
            Records = records.Skip((pageNumber - 1) * perPage).Take(perPage).ToList();

            PagingInfo.TotalRecords = records.Count();
            PagingInfo.CurrentPage = pageNumber;
            PagingInfo.PerPage = perPage;
        }

        public PaginatedRecord(List<T> records, int totalRecordsCount, int pageNumber = 1, int perPage = 10)
        {
            Records = records;

            PagingInfo.TotalRecords = totalRecordsCount;
            PagingInfo.CurrentPage = pageNumber;
            PagingInfo.PerPage = perPage;
        }

        public List<T> Records { get; set; }
        public PagingInfo PagingInfo { get; private set; } = new PagingInfo();
    }

    public class PagingInfo
    {
        public int PerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalRecords { get; set; }
        // Adding the value using mod is considerably faster than Math.Ceiling, which returns a Decimal which would then need to be converted to an int
        public int TotalPages => (PerPage == 0) ? 0 : (TotalRecords / PerPage) + (TotalRecords % PerPage == 0 ? 0 : 1);

        public bool IsValid()
        {
            return CurrentPage >= 1 && CurrentPage <= TotalPages;
        }
    }
}
