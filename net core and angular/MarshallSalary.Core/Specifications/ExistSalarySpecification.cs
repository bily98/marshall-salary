﻿using Ardalis.Specification;
using MarshallSalary.Core.Entities;
using NodaTime;

namespace MarshallSalary.Core.Specifications
{
    public class ExistSalarySpecification : Specification<Employee>, ISingleResultSpecification
    {
        public ExistSalarySpecification(string employeeCode, Instant date)
        {
            var zoned = date.InUtc();

            Query.Where(x => x.EmployeeCode == employeeCode &
                x.Year == zoned.Year &
                x.Month == (zoned.Month));
        }
    }
}
