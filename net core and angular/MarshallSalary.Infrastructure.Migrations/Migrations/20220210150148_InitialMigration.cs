﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MarshallSalary.Infrastructure.Migrations.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "division",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_division", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "office",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_office", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "position",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_position", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    year = table.Column<int>(type: "int", nullable: false),
                    month = table.Column<int>(type: "int", nullable: false),
                    office_id = table.Column<int>(type: "int", nullable: false),
                    employee_code = table.Column<int>(type: "int", nullable: false),
                    employee_name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    employee_surname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    division_id = table.Column<int>(type: "int", nullable: false),
                    position_id = table.Column<int>(type: "int", nullable: false),
                    grade = table.Column<int>(type: "int", nullable: false),
                    begin_grade = table.Column<DateTime>(type: "datetime2", nullable: false),
                    birthday = table.Column<DateTime>(type: "datetime2", nullable: false),
                    identification_number = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    base_salary = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    production_bonus = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    compensation_bonus = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    commission = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    contributions = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_employee", x => x.id);
                    table.ForeignKey(
                        name: "fk_employee_division_division_id",
                        column: x => x.division_id,
                        principalTable: "division",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_employee_offices_office_id",
                        column: x => x.office_id,
                        principalTable: "office",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_employee_positions_position_id",
                        column: x => x.position_id,
                        principalTable: "position",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_employee_division_id",
                table: "employee",
                column: "division_id");

            migrationBuilder.CreateIndex(
                name: "ix_employee_office_id",
                table: "employee",
                column: "office_id");

            migrationBuilder.CreateIndex(
                name: "ix_employee_position_id",
                table: "employee",
                column: "position_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "employee");

            migrationBuilder.DropTable(
                name: "division");

            migrationBuilder.DropTable(
                name: "office");

            migrationBuilder.DropTable(
                name: "position");
        }
    }
}
